Why are you unsure if you will vote or not? Choose one or more options below.

	Because I don’t know if I am eligible.
	Because I don’t understand how to vote.
	Because I don't know who I should vote for. 
	Because I don’t see any point in voting.
	Because I am very busy.
	Because I am worried about my security.
	Because my friends/family members say I shouldn't.
	Because I doubt I can change anything.
	Because I haven’t yet found any political leaders that share my values.
	Because I don't know where the polling stations are.
	Another reason
