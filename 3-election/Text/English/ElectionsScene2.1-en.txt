HSU MYAT:
Actually, there are lots of leaders to choose from in the elections… I'm sure one of them also cares about the traffic.

ZAW TINT:
I doubt that!

WAI PHYO:
But, in the previous elections… didn't you vote then either?

ZAW TINT:
Why would I have voted? Those candidates don’t seem to ever agree on anything. One person says something, and the next says something different. How could they change anything if they can't even say the same thing!

HSU MYAT:
How do you mean?

ZAW TINT:
So I heard on the radio one saying he would get more foreign companies to come to Myanmar, to build factories. Then another said he would spend money on local farmers. I don't get it! Are they saying we should have more factories or more farmers? 
