Why are you planning not to vote? Choose one or more options below.

	Because I am not eligible.
	Because I don’t understand how to vote.
	Because I don't know who I should vote for. 
	Because there's no point in voting.
	Because I am too busy.
	Because I am worried about my security.
	Because my friends/family members say I shouldn't.
	Because I don't think I can change anything.
	Because no political leader shares my values.
	Because I don't know where the polling stations are.
	Another reason
