Once all members of the Union Assembly have been elected, they are responsible for choosing the President.

Each of the two assemblies in the Union Assembly nominate a Presidential candidate. The military members of parliament (MPs) also nominate one Presidential candidate.

Then all members of the Union Assembly and all the military MPs meet to vote on who the President should be. The nominated candidate who wins the most votes becomes the President. The two nominated candidates that get fewer votes become Vice-Presidents. 

This means that the more members a party has in the Union Assembly, the more influence it can have on electing the President. 
