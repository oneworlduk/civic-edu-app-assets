ဆုျမတ္:
ပါတီေတြက အမ်ားအားျဖင့္ ကိစၥရပ္ၾကီးေတြကိုပဲ အေလးထားတာမ်ားတယ္။ ျမန္မာႏိုင္ငံကို ဘယ္လိုဖြံျဖိဳးတိုးတက္ေအာင္လုပ္မလဲဆိုတာမ်ိဳးေပါ့။ ဒါေပမယ့္လည္း ဒီလိုျမိဳ႔ၾကီးမွာ လမ္းပန္းဆက္သြယ္ေရးက အရမ္းအေရးၾကီးတာေၾကာင့္ ဒီျပႆနာကိုလည္း ေျဖရွင္းေပးလိမ့္မယ္ထင္ပါတယ္။

ေဇာ္တင့္:
ဦးေလးအေနနဲ႔ကေတာ့ ဒီပါတီေတြထဲကတစ္ခုမွကို အယံုအၾကည္မရွိဘူး။ ေရြးေကာက္ပြဲမတိုင္ခင္ ပါတီအသစ္ေတြအမ်ားၾကီးေပၚလာေၾကာင္းေျပာေနေပမယ့္လည္း ဘယ္လုိလုပ္ယံုရမလဲ။ ဘာမွ ရလဒ္ေတြကိုၾကည့္လို႔မရဘူး။ ဘယ္နည္းနဲ႔မဆို ေရြးေကာက္ပြဲမွာႏိုင္သြားႏိုင္တာပဲ။ ပါတီအတုေတြရွိတာထက္ေတာ့ ပိုေကာင္းပါတယ္။

ဆုျမတ္:
တကယ္ေတာ့ ပါတီအသစ္အေနနဲ႔ စာရင္းတင္သြင္းဖို႔ကအရမ္းရႈပ္ေတြးပါတယ္။ စာရင္းတင္သြင္းတာေအာင္ျမင္ဖို႔ေတာ္ေတာ္ကို ခဲခဲယဥ္းယဥ္းလုပ္ရမွာ။ အမွန္အတိုင္းေျပာရရင္ ပါတီအတုေတြ ျမန္မာႏိုင္ငံမွာ ရွိတယ္လို႔ မျမင္မိပါဘူး

ေဇာ္တင့္:
ေအာ္ သမီးကငယ္ေသးတာကို။ ဦးကဘ၀မွာအမ်ားၾကီးျဖတ္သန္းလာရတာ။ မဲေပးေပး မေပးေပး ဘာမွမျဖစ္ပါဘူး။

ဆုျမတ္:
ဟုတ္ပါတယ္ တခ်ိဳ႔ပါတီေတြက တျခားပါတီေတြေလာက္ေတာ့ ေလးေလးနက္နက္မရွိၾကဘူး။ ဦးေလးအေနနဲ ့လႊတ္ေတာ္မွာ အုပ္ခ်ဳပ္ဖို႔ အေကာင္းဆံုးပါတီကို ေရြးခ်ယ္ခြင့္ရွိတာပဲ

ေဇာ္တင့္:
အင္း ဒါလည္းဟုတ္တာပဲ
