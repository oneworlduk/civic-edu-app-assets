Which of the 4 options is correct?

The separation of powers between different branches is important so that
	citizens can vote on the President
	the police can also make laws
	no branch gets too much power
	every branch is equally weak under the President

(Feedback)
The separation of powers between different branches of the government is very important to ensure that no branch gets too much power over how the country is ruled. If one branch had more power than the others, it could threaten the stability and democracy of the country.
