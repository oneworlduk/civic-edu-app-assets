HSU MYAT:
I think you’re wrong. I think we need leaders that the people have chosen – not just strong leaders.

ROSY:
But strong leaders are good at making decisions. I'd rather trust them than let those uneducated people make decisions for our country.

WAI PHYO:
But, can I ask you one thing?

ROSY:
Of course, Wai Phyo! YOU can ask me ANYTHING!

WAI PHYO: 
How would you feel if someone decided what you should eat for dinner every day? 

ROSY:
What?

WAI PHYO:
Like, pork curry, for example?

ROSY:
That'd be unfair! I hate pork!

WAI PHYO:
You see? 

HSU MYAT:
That's exactly why we need democracy. People should have the right to make decisions – and have many options to choose from. Otherwise we end up having an authoritarian system!

ROSY:
Authori… What?!

HSU MYAT:
An authoritarian system. That's basically a system in which we are left with no options – because the leaders choose for us.
