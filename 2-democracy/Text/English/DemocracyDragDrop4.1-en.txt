Put the sentences below in order from most important (1) to least important (4). There are no right or wrong answers.

GAME 1

	People from minority groups should also be involved in ruling the country.
	Protecting citizens' rights is an important part of democracy.
	Everyone is responsible to maintain a democracy.
	People should be able to choose if they want to have democratic leaders or authoritarian leaders.
