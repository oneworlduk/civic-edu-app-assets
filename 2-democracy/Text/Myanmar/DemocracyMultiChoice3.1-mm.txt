ေအာက္တြင္ေပးထားေသာေရြးခ်ယ္စရာ ၄ခုမွ မွန္ကန္ေသာတစ္ခုကိုေရြးပါ။

အစိုးရအဖြဲ႔တြင္ အာဏာအရွိဆံုးအဖြဲ႔သည္။
−	တရားဥပေဒစိုးမိုးေရးအဖြဲ႔
−	ဥပေဒခ်မွတ္ေရးအဖြဲ႔
−	တရားစီရင္ေရးအဖြဲ႔
−	အဖြဲ႔အားလံုးတူညီေသာအာဏာရွိသည္။

(ေကာက္ႏုတ္ခ်က္)
အစိုးရ၏အဖြဲ႔ခြဲသံုးဖြဲ႔တြင္ အားလံုးတူညီေသာ အာဏာရွိၾကသည္။ အေရးပါေသာ တာဝန္ရပ္အသီးသီးကိုလုပ္ေဆာင္ေနၾကရျပီး အဖြဲ႔သံုးဖြဲ႔လံုးတြင္ ႏိုင္ငံကိုအုပ္ခ်ဳပ္ရာတြင္ မည္သည့္အဖြဲ႔တြင္  အာဏာပိုရျခင္းမရွိရန္ အျခားအဖြဲ႔မွ လုပ္ေဆာင္ခ်က္မ်ားကို စစ္ေဆးၾကည့္ရႈပိုင္ခြင့္မ်ားရထားၾကသည္။
