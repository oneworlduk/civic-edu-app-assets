
ေဝျဖိဳး။   ။ ဒါ ေတာ္ေတာ္ေကာင္းတာပဲ ။ ငါလည္း ျမန္မာႏိုင္ငံကို ဒီလို ျဖစ္ေစခ်င္တယ္။

ဆုျမတ္။ ။ ေျပာပါအံုး ။ ျမန္မာႏိုင္ငံအတြက္ နင့္ရဲ ့တျခားအိပ္မက္ေတြ ရွိလား ?

ေဝျဖိဳး။  ။ တခါတေလ..ငါလုပ္ခ်င္တာေတြ ရွိတယ္။ နင္သိလား ။ ငါ့ ပတ္ဝန္းက်င္ ကို ငါေျပာင္းလဲခ်င္တာ အမ်ားၾကီးရွိတယ္ဟ။ျပီးေတာ့ ငါထင္တာက ဒီက အဖြဲ ့အစည္းေတြကလည္း အဲ့လိုပဲထင္တယ္။ ဒါေပမဲ့.. အကယ္လို ့ ငါတို ့အားလံုး ဒီပတ္ဝန္းက်င္ကို ပိုေကာင္းေအာင္
ေျပာင္းလဲႏိုင္ရင္ ၊ ျမန္မာႏိုင္ငံၾကီးက ဘာျဖစ္လာမလဲ ? ငါကိုယ္တိုင္ ျမန္မာႏိုင္ငံသား ျဖစ္ရတာကို
အရင္ကထက္ ပိုျပီး ဂုဏ္ယူမိမယ္။

ဆုျမတ္။  ။ နင့္ အိပ္္မက္ကို ငါ ၾကိဳက္တယ္။

ေဝျဖိဳး။  ။ ငါကေတာ့ ဒါဟာ အိပ္မက္တစ္ခုတင္မဟုတ္ဘူး ဆိုတာ။ အခ်ိန္တိုင္း ပိုေကာင္းေအာင္ လုပ္ဖို ့စဥ္းစားေနေသးတယ္။နင္သိလား.. လြန္ခဲ့တဲ့ ႏွစ္အနည္းငယ္က ငါတို ့ျမိဳ ့မွာ လွ်ပ္စစ္မီး ဆိုတာ မရွိဘူးေလ။ နင္ US မွာ ေနတုန္းက TV မွာလည္း ၾကည့္မိမွာပဲ။
ငါတို ့အိမ္ေတြမွာ လွ်ပ္စစ္မီးေတာင္ မရွိဘူး။ ဒါေပမဲ့ အခု ငါတို ့မွာ လွ်ပ္စစ္မီးရေနျပီ အိမ္ေတြမွာ ၊ လမ္းေတြမွာ ၊ ေနရာတကာမွာေလ။

ဆုျမတ္။  ။ ဒါက ေတာ္ေတာ္ေလး ၾကီးတဲ့ အေျပာင္းအလဲပဲ။ အဲ့ဒါ ဘယ္လိုျဖစ္သြားတာလဲ ?

ေဝျဖိဳး။  ။ ငါ့ မိသားစု နဲ ့ တျခားမိသားစုေတြအားလံုး ေျပာၾကတယ္။ ငါတို ့လွ်ပ္စစ္မီးလိုခ်င္တယ္ 
ဆိုျပီးေတာင္းဆိုၾကတယ္ေလ။ အခု ငါတို ့ရျပီေလ။

ဆုျမတ္။ ။ အဲ့ဒါပဲ.. တက္ၾကြႏိုင္ငံသားဆိုတာ..

ေဝျဖိဳး။  ။ ဟားဟားး.. ငါထင္တာကေတာ့..ဒါက အလုပ္ျဖစ္သားပဲေနာ္။
