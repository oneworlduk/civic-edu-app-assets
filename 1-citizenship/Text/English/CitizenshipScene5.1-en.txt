WAI PHYO:
Hsu Myat… even though you say you want to live here, do you sometimes think things were better in the U.S.?

HSU MYAT:
Well, sometimes I do.

WAI PHYO:
I would really like to know what the U.S. is like… I mean, in real life. Not like in the movies.

HSU MYAT:
Well, there are lots of TV shows and magazines in the U.S. I liked that. Sometimes I just watched TV for fun; sometimes I read news about the world. There was just so much entertainment and information all around me, all the time!

WAI PHYO:
Wow.

HSU MYAT:
Yeah, and I improved my English a lot. That's also another thing. I sometimes felt like I had more opportunities in the U.S. But then again…

WAI PHYO:
Then again what?

HSU MYAT:
… Then there is this whole thing with citizenship and belonging. I want to stay a Myanmar citizen, and stay here, now that I’ve moved back. Sometimes I think I should be trying to change Myanmar, rather than looking back at my time in the U.S. Change Myanmar so that people feel like they also have lots of opportunities here.
