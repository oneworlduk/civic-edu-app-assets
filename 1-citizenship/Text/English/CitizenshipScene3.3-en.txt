WAI PHYO:
I really would like to know how to improve my community... I really would.

HSU MYAT:
So, what about that blood donation group? Can't you join them?

WAI PHYO:
Actually, I don't know them too well...

HSU MYAT:
But you are interested in what they do?

WAI PHYO:
I guess so.

HSU MYAT:
Then I think you should contact them! Actually, active citizenship is also about doing something you are passionate about. Only you know what's important to you and how you would like to change your community!
