There are 3 types of human rights.

First, civil and political rights give citizens a right to a nationality, equality to participate in the political system, and a right to live free from discrimination.

Second, economic, social and cultural rights allow citizens to work and earn an income, to have good housing, and to practice a religion, or to choose not to have any religion. 

Third, solidarity rights are collective rights that apply to communities. Solidarity rights ensure that citizens can live in a safe and healthy environment and in a world with no discrimination between peoples of different groups or countries.
