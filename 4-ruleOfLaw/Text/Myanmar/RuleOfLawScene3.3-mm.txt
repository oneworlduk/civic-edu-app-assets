ဆရာသန္႔ဇင္:
ဒါဆိုေက်ာင္းကစည္းကမ္းေတြက ဘာလို႔ ဥပေဒေတြမျဖစ္ႏိုင္လဲသိျပီမဟုတ္လား။?

ေ၀ျဖိဳး:
ဟုတ္ကဲ့ပါ။...

ဆရာသန္႔ဇင္:
ေက်ာင္းကစည္းကမ္းေတြကို အိမ္မွာေတာင္လိုက္နာဖို႔မလိုတာ တစ္ႏိုင္ငံလံုးဘာလို႔လိုက္နာဖို႔လိုမွာလဲ။

ေ၀ျဖိဳး:
ဆရာေျပာတာ မွန္ပါတယ္။

ဆုျမတ္:
ေက်ာင္းမွာ စာသင္ခ်ိန္မွာမုန္႔စားတာေၾကာင့္ တရားရံုးမွာ ေက်ာင္းစည္းကမ္းေတြကိုခ်ိဳးေဖာက္လို႔ အျပစ္ေပးခံရမွာမဟုတ္ဘူး။ ေက်ာင္းက ေခါင္းေဆာင္ေတြသတ္မွတ္ထားတဲ့ အရာေတြပဲေလ။

ဆရာသန္႔ဇင္:
ေက်ာင္းက ေခါင္းေဆာင္ဆိုေတာ့ ဆရာလိုမ်ိဳးကိုေျပာတာလား။

ဆုျမတ္:
ဟားဟား ဟုတ္ပါတယ္ ဆရာကိုေျပာတာပါ။

ဆရာသန္႔ဇင္:
မင္းသိလား ေ၀ျဖိဳး ေက်ာင္းက စည္းကမ္းေတြက အစိုးရရဲ႕ဥပေဒ ေတြလိုမ်ိဳးပဲ ဒါေပမယ့္ ဥပေဒေတြက အစိုးရစည္းမ်ဥ္းစည္းကမ္းေတြနဲ႔ ေက်ာင္းစည္းကမ္းေတြရဲ႕အထက္ မွာပဲရွိတယ္။

ေ၀ျဖိဳး:
ႏိုင္ငံေရးလား။?
