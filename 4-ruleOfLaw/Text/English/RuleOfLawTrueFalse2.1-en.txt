Is this statement true or false?

“The leader of a country does not have to obey the rule of law.”

(When “TRUE” is clicked, this answer is provided)
WRONG ANSWER. Every citizen of the country must obey the rule of law, including politicians and other leaders. The rule of law ensures that no one is above the law and every citizen is equal under the law.

(When “FALSE” is clicked, this answer is provided)
CORRECT ANSWER. Every citizen of the country must obey the rule of law, including politicians and other leaders. The rule of law ensures that no one is above the law and every citizen is equal under the law.
