Is this statement true or false?

“Justice means that people can take revenge when they feel mistreated.”

(When “TRUE” is clicked, this answer is provided)
WRONG ANSWER. Justice means that people should be treated in a fair way. But fairness depends on equality. In a just system, everyone has a right not to be mistreated. In a just system, anyone who mistreats someone else should also be punished under the law.

If people feel that their rights have been violated, they should not take justice in their own hands, but let a judge in the legal system decide how to correct this situation. Only an impartial judiciary can ensure a fair punishment for those who break the law.

(When “FALSE” is clicked, this answer is provided)
CORRECT ANSWER. Justice means that people should be treated in a fair way. But fairness depends on equality. In a just system, everyone has a right not to be mistreated. In a just system, anyone who mistreats someone else should also be punished under the law.

If people feel that their rights have been violated, they should not take justice in their own hands, but let a judge in the legal system decide how to correct this situation. Only an impartial judiciary can ensure a fair punishment for those who break the law.
