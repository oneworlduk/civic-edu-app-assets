VOICE-OVER:
SAYAR THANT ZIN, HSU MYAT and WAI PHYO'S beloved teacher, walks up slowly and sits with them.

WAI PHYO:
Sayar! How are you?

SAYAR THANT ZIN:
Not bad at all! And who’s this… Oh my, is that Hsu Myat?

HSU MYAT:
You remember me! Hello, Sayar Thant Zin. How are you?

SAYAR THANT ZIN:
Of course! I remember my students. I’m doing well; I’m the principal now. What are you two up to?

WAI PHYO:
We’re just chatting about our school days…

HSU MYAT:
And about how some of the teachers back then abused their power.

SAYAR THANT ZIN:
Is that so? What did they do to you?
