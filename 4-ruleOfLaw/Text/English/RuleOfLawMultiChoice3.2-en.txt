Which of the 4 options is correct?

Laws are …………… than a policy.
-	More important
-	More changeable 
-	More temporary
-	More permanent

(Feedback)
Laws are more permanent than policies. A law usually doesn’t change when there is a new government, but the policy of the ruling government does. The government must follow a specific process to change laws, and that is also why laws are more permanent.
