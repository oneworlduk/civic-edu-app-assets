SAYAR THANT ZIN:
Well, even super heroes have their ways of dealing with problems. And you know, as principal of this school, I need to ensure I'm being fair to everyone. Even to bullies.

HSU MYAT:
Why would you want to be fair to them? They've done wrong!

SAYAR THANT ZIN:
That may be true. But in some cases, students have fought with each other and there was actually no one-sided bullying. That is what I need to sort out first. You know, I have a principle I try to follow as strictly as I can: "everyone should be assumed innocent until proven guilty".

WAI PHYO:
I recognize that from TV!

HSU MYAT:
And I'm quite sure I've heard it at the university before!

SAYAR THANT ZIN:
That's probably so. It's a principle that’s very important for the rule of law. And it applies to my school, too.

WAI PHYO:
I see. But, Sayar, what if I'm doing everything right?

SAYAR THANT ZIN:
Like how?

WAI PHYO:
Like… what if I never break any laws or rules in school? Why would the rule of law matter to me?

HSU MYAT:
But it matters to everyone!

WAI PHYO:
If you only get to know what the rule of law is when you actually break a law or a rule, why should everyone else also care about it?

HSU MYAT:
Wai Phyo, if you need to get to the other side of a street, how do you cross it?

WAI PHYO:
By walking…?

HSU MYAT:
OK. But if there's a traffic sign, and if it's red…

WAI PHYO:
…Then I wouldn't cross the street until it turns green for me.

HSU MYAT:
Why don't you cross the street when the traffic sign is red?

WAI PHYO:
Because that would be dangerous! You saw yourself how irresponsibly our taxi driver drove us here!

HSU MYAT:
So this means that even though you have no intention to break traffic rules, you need to know about them? Only by knowing the rules you can make a decision to follow them, to stay safe?

WAI PHYO:
Umm… I guess you're right, Hsu Myat.

HSU MYAT:
You see? The rule of law matters to everyone – even the most law-abiding person like you!
