This repo contains the image, audio and text assets used in OneWorld's Myanmar Civic Education App.

Currently it is structured as follows:

- citizenship, election, democracy, peace and ruleOfLaw folders
  Currently these just contain the visual design files (.psd) for the intro page for each chapter.
  Eventually they will contain all the text, image and audio files for each chapter.

- intro folder
  Contains the visual design files for the four intro screens, and the icons for those screens

- mainMenu folder
  Contains the visual design file for the MainMenu screen, and the icons for that screen

- fonts folder
  Font files for use in the app