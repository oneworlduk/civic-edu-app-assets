ROSY:
Actually, I might join a sports club.

HSU MYAT:
You will?! I didn't know you liked sports.

ROSY:
Of course I do. My father is crazy about sports, so I got it from him. Don't judge me by my looks!

HSU MYAT:
Ha, ha. That's true – no one should judge anyone else just on their appearance.

YOON:
Or on their religion or ethnicity.

ROSY:
You know what, Yoon? I think I understand you now.

YOON:
That’s great.

ROSY:
I'm honestly very happy I met you. I feel…

SAYAR THANT ZIN:
… inner peace?

ROSY, WAI PHYO, HSU MYAT, YOON, SAYAR THANT ZIN:
Ha, ha.

ROSY:
Well, yes, that's true. But I also feel like… maybe I can do something that’s truly good for my country. Maybe it's not as hard as I thought it was.

YOON:
We can all play a part in creating peace in Myanmar.

SAYAR THANT ZIN:
It’s all in our hands.

HSU MYAT, WAI PHYO, ROSY, YOON:
It’s all in our hands!
